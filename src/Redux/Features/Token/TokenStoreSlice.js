import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    id: JSON.parse(sessionStorage.getItem('auth')) ? JSON.parse(sessionStorage.getItem('auth')).userId : JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).userId : "",
    accessToken: JSON.parse(sessionStorage.getItem('auth')) ? JSON.parse(sessionStorage.getItem('auth')).access_token : JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')).access_token : ""
}

const sessionSlice = createSlice({
    name: 'sessionStorage',
    initialState,
    reducers: {
        setToken: (state, action) => {
            state.accessToken = action.payload.accessToken
            state.id = action.payload.id
            action.payload.rememberMe ? localStorage.setItem('auth', JSON.stringify({ access_token: action.payload.accessToken, userId: action.payload.id }))
                : sessionStorage.setItem('auth', JSON.stringify({ access_token: action.payload.accessToken, userId: action.payload.id }))
        },
        removeToken: (state) => {
            state.accessToken = ""
            state.id = ""
            localStorage.removeItem('auth')
            sessionStorage.removeItem('auth')
        }
    }
})

export const { setToken, removeToken } = sessionSlice.actions

export default sessionSlice.reducer;