import { configureStore } from "@reduxjs/toolkit";
import tokenReducer from "../Features/Token/TokenStoreSlice"

export const store = configureStore({
    reducer: {
        token: tokenReducer
    }
})