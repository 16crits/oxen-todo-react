import { NavBar, Todos } from "./Components";

export function Index() {
  return (
    <div className="grow">
      <NavBar />
      <Todos />
    </div>
  );
}
