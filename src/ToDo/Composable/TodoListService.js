import axios from "axios"
import { useState } from "react"
import { useSelector } from "react-redux"
import { API_URL } from "../../common"

export function TodoListService() {

    const { id } = useSelector((state) => state.token)
    const [status, setStatus] = useState('')
    const [todo, setTodo] = useState('')
    const url = `${API_URL}/api/to-dos`

    function saveTodo(title, deadLine) {
        axios.post(url, {
            title, deadLine, userId: id
        }).then(response => {
            setStatus(200)
            setTodo(response.data)
        }).catch(error => {
            console.log(error.response)
        })
    }

    function deleteTodo(id) {
        axios.delete(`${url}/${id}`).then(response => {
            setStatus(200)
        }).catch(error => {

        })
    }

    function updateTodo(id, title, deadLine, isDone = false) {
        setStatus('')
        axios.put(`${url}/${id}`, {
            title, deadLine, isDone, userId: id
        }).then(response => {
            setStatus(200)
        })
    }

    return { saveTodo, updateTodo, status, todo, deleteTodo }
}