import axios from "axios";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { API_URL } from "../../common";
import { removeToken } from "../../Redux/Features/Token/TokenStoreSlice";

export function useTodoListRepository() {

    const [status, setStatus] = useState('')
    const [todos, setTodos] = useState([])
    const [pages, setPages] = useState([])
    const { accessToken } = useSelector((state) => state.token)
    const dispatch = useDispatch()


    function getTodoList(URL, todos = 'active') {
        axios.get(URL ? URL : `${API_URL}/api/to-dos?todos=${todos}`, {
            headers: {
                'Authorization': `Bearer ${accessToken}`
            }
        }).then(response => {
            setTodos(response.data.data)
            setPages(response.data.meta.links)
            setStatus(200)
        }).catch(error => {
            if (error.response.status === 401) {
                dispatch(removeToken())
                setStatus(401)
            }
        })
    }

    function editTodo(index, title = "", deadLine = "", isDone = false) {
        let todosDupe = [...todos];
        let todo = todosDupe[index]
        todo.title = title;
        todo.dead_line = deadLine;
        todo.is_done = isDone;
        setTodos(todosDupe)

    }

    async function sortTodos(todos) {
        await new Promise((resolve, reject) => {
            todos.sort((first, second) => {
                if (first.id && second.id) {
                    if (first.dead_line > second.dead_line) {
                        return 1;
                    }
                    if (first.dead_line < second.dead_line) {
                        return -1;
                    }
                }

            });
            resolve(true)
        }).then(resolve => {
            setTodos(todos);
        });
    }

    function newTodo(todo) {
        let dupeTodos = [...todos]
        dupeTodos.push(todo)
        sortTodos(dupeTodos)
    }

    return { getTodoList, status, todos, pages, editTodo, newTodo, sortTodos }
}