import { useEffect, useState } from "react";
import { TodoListService } from "../Composable";

export function Todo({ todo, getTodoList, tab }) {
  const [edit, setEdit] = useState(false);
  const [title, setTitle] = useState(todo.title);
  const [deadLine, setDeadLine] = useState(todo.dead_line);
  const { updateTodo, status, deleteTodo } = TodoListService();

  useEffect(() => {
    setTitle(todo.title);
    setDeadLine(todo.dead_line);
  }, [todo]);

  useEffect(() => {
    if (status === 200) {
      setEdit(false);
      getTodoList();
    }
  }, [status]);

  const cancelEdit = () => {
    setEdit(false);
  };
  return (
    <div className="flex-1">
      {!edit ? (
        <div className="todo-row">
          <div className="flex-1">
            <span>{todo.title}</span>
          </div>
          <div className="flex-1">
            {tab === "active" ? (
              <span>
                {todo.dead_line ===
                new Date().toLocaleDateString("en-ZA").replaceAll("/", "-")
                  ? "Today"
                  : todo.dead_line}
              </span>
            ) : (
              <span>{todo.is_done ? "Done" : "Expired"}</span>
            )}
          </div>
          {tab === "inactive" && (
            <div className="flex-1">
              <span>
                {todo.is_done
                  ? new Date(todo.updatedAt).toLocaleDateString("en-ZA")
                  : "-"}
              </span>
            </div>
          )}
          <div className="flex flex-1 items-center space-x-6">
            {tab === "active" && (
              <i
                className="fa-solid fa-pen-to-square cursor-pointer text-blue-500"
                title="Edit"
                onClick={() => {
                  setEdit(true);
                }}
              ></i>
            )}
            {tab === "active" && (
              <i
                className="fa-solid fa-list-check cursor-pointer text-red-500"
                title="Mark as done"
                onClick={() =>
                  updateTodo(todo.id, todo.title, todo.dead_line, true)
                }
              ></i>
            )}

            <i
              className="fa-solid fa-trash-can cursor-pointer text-red-500"
              title="delete"
              onClick={() => {
                deleteTodo(todo.id);
              }}
            ></i>
          </div>
        </div>
      ) : (
        <div className="todo-row">
          <div className="flex-1">
            <input
              type="text"
              onChange={(e) => setTitle(e.target.value)}
              value={title}
            />
          </div>
          <div className="flex-1">
            <input
              type="date"
              onChange={(e) => setDeadLine(e.target.value)}
              min={new Date().toLocaleDateString("en-ZA").replaceAll("/", "-")}
              value={deadLine}
            />
          </div>
          <div className="flex flex-1 space-x-2">
            <button
              className="submit"
              onClick={() => updateTodo(todo.id, title, deadLine)}
            >
              Save
            </button>

            <button
              className="cancel"
              onClick={() => {
                cancelEdit();
              }}
            >
              Cancel
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
