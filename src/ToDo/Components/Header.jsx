export function Header({ tab }) {
  const headers =
    tab === "active"
      ? ["Task", "Dead Line", "Actions"]
      : ["Task", "Status", "Done At", "Actions"];
  return (
    <div className="flex border-b border-gray-500">
      {headers.map((header, index) => {
        return (
          <div className="flex-1" key={index}>
            {header}
          </div>
        );
      })}
    </div>
  );
}
