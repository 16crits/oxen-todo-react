export function Tabs({ getTodoList, currentTab, setTab }) {
  const tabs = [
    { key: "active", value: "To Do" },
    { key: "inactive", value: "Expired/Completed" },
  ];
  return (
    <div className="flex w-min" >
      {tabs.map((tab) => {
        return (
          <div
            key={tab.key}
            className={
              currentTab === tab.key
                ? "tab-active"
                : "tab"
            }
            onClick={() => {
              setTab(tab.key);
              getTodoList("", tab.key);
            }}
          >
            {tab.value}
          </div>
        );
      })}
    </div>
  );
}
