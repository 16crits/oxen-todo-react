import { useDispatch } from "react-redux";
import { removeToken } from "../../Redux/Features/Token/TokenStoreSlice";

export function NavBar() {
  const disptach = useDispatch();

  return (
    <div className="flex p-4 bg-blue-500">
      <div className="grow text-white">
        <span>Oxen's To-Do List</span>
      </div>
      <div>
        <button onClick={() => disptach(removeToken())}>Logout</button>
      </div>
    </div>
  );
}
