import { useEffect, useState } from "react";
import { TodoListService } from "../Composable";

export function CreateTodo({ setAddingFalse, newTodo, getTodoList }) {
  const [title, setTitle] = useState("");
  const [deadLine, setDeadLine] = useState(
    new Date().toLocaleDateString("en-ZA").replaceAll("/", "-")
  );
  const { saveTodo, status, todo } = TodoListService();

  useEffect(() => {
    if (status === 200) {
      setAddingFalse();
      getTodoList();
    }
  }, [status, todo]);
  return (
    <div className="todo-row">
      <div className="flex-1">
        <input
          type="text"
          onChange={(e) => setTitle(e.target.value)}
          value={title}
        />
      </div>
      <div className="flex-1">
        <input
          type="date"
          onChange={(e) => setDeadLine(e.target.value)}
          min={new Date().toLocaleDateString("en-ZA").replaceAll("/", "-")}
          value={deadLine}
        />
      </div>

      <div className="flex flex-1 space-x-2">
        <button
          className="submit"
          onClick={() => {
            saveTodo(title, deadLine);
          }}
        >
          Save
        </button>

        <button
          className="cancel"
          onClick={() => {
            setAddingFalse();
          }}
        >
          Cancel
        </button>
      </div>
    </div>
  );
}
