import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { useTodoListRepository } from "../Composable";
import { Header, Todo, CreateTodo, Tabs } from "./index";
import "../Scss/index.scss";

export function Todos() {
  const { getTodoList, status, todos, newTodo, pages } =
    useTodoListRepository();
  const [redirect, setRedirect] = useState(false);
  const [adding, setAdding] = useState(false);
  const [tab, setTab] = useState("active");
  const { accessToken } = useSelector((state) => state.token);

  useEffect(() => {
    getTodoList();
  }, []);

  useEffect(() => {
    if (status === 401) {
      setRedirect(true);
    }
  }, [status]);
  return redirect || !accessToken ? (
    <Navigate to="/" />
  ) : (
    <div className=" w-full p-4">
      <Tabs
        getTodoList={getTodoList}
        setTab={(tab) => setTab(tab)}
        currentTab={tab}
      />
      <div className="bg-white rounded-lg p-4 grid gap-y-2">
        <Header tab={tab} />
        <div>
          {todos.map((todo) => {
            return (
              <Todo
                todo={todo}
                key={todo.id}
                getTodoList={getTodoList}
                tab={tab}
              />
            );
          })}
        </div>

        {adding && tab !== "inactive" && (
          <CreateTodo
            setAddingFalse={() => setAdding(false)}
            newTodo={newTodo}
            getTodoList={getTodoList}
          />
        )}
        {!adding && tab !== "inactive" && (
          <div className="flex  items-center">
            <button
              className=" space-x-2 bg-blue-500 p-2 rounded-lg"
              onClick={() => {
                setAdding(true);
              }}
            >
              <i className="fa-solid fa-plus"></i>
              <span>Add new todo</span>
            </button>
          </div>
        )}

        <div className="pagination">
          {pages.length > 1 &&
            pages.map((page, index) => {
              return (
                <div
                  className="page"
                  onClick={() => getTodoList(page)}
                  key={index}
                >
                  {index + 1}
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}
