import { useState } from "react";
import { Link, Navigate } from "react-router-dom";
import "./Scss/index.scss";
import useLoginRepository from "./Composable/useLoginRepository";

export function Login() {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(false);
  const { login, status, errMsg } = useLoginRepository();

  return status === 200 ? (
    <Navigate to="/dashboard" />
  ) : (
    <form className="auth-wrapper" onSubmit={(e) => e.preventDefault()}>
      <div>
        <span>Welcome!</span>
      </div>
      <div>
        <span>
          Login or <Link to="/register">register</Link> to continue.
        </span>
      </div>
      <div>
        <span className="text-red-500">{errMsg}</span>
      </div>
      <div>
        <input
          type="text"
          required
          onChange={(e) => setUserName(e.target.value)}
        />
      </div>
      <div>
        <input type="password" onChange={(e) => setPassword(e.target.value)} />
      </div>
      <div className="flex space-x-2">
        <input
          type="checkbox"
          onChange={(e) => setRememberMe(e.target.checked)}
        />
        <span>Remember me</span>
      </div>
      <div>
        <button
          disabled={!password || !userName ? true : false}
          className="submit"
          onClick={() => login(userName, password, rememberMe)}
        >
          Login
        </button>
      </div>
    </form>
  );
}
