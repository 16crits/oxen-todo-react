import axios from "axios";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { API_URL } from "../../common";
import { setToken } from "../../Redux/Features/Token/TokenStoreSlice";

export default function useRegisterRepostiory() {

    const [status, setStatus] = useState('')
    const dispatch = useDispatch()
    function register(name, email, password) {
        axios.post(`${API_URL}/api/auth/sign-up`, {
            name, email, password
        }).then(response => {
            dispatch(setToken({
                accessToken: response.data.access_token, id: response.data.id, rememberMe: false
            }))
            setStatus(200)
        }).catch(error => {
            setStatus(401)
        })
    }

    function validateEmail(email) {

        return email ? /\S+@\S+\.\S+/.test(email) : true;
    }

    return { register, validateEmail, status }
}