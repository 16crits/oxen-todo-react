import axios from "axios"
import { API_URL } from "../../common.js"
import { useState } from "react";
import { useDispatch } from "react-redux";
import { setToken } from "../../Redux/Features/Token/TokenStoreSlice.js";
export default function useLoginRepository() {

    const [status, setStatus] = useState('')
    const [errMsg, setErrMsg] = useState('')
    const dispatch = useDispatch();

    function login(emailOrName, password, rememberMe) {
        axios.post(`${API_URL}/api/auth/sign-in`, {
            emailOrName,
            password
        }).then(response => {
            dispatch(setToken({ accessToken: response.data.access_token, id: response.data.id, rememberMe }));
            setStatus(200)
        }).catch(error => {
            setStatus(403)
            setErrMsg(error.response.data.message)
        })

    }

    return { login, status, errMsg };
}