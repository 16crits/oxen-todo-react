export function Password({ handlePasswordChange }) {
  return (
    <div>
      <label htmlFor="password">
        Password <span className="text-red-500">*</span>
      </label>
      <input
        id="password"
        type="password"
        onChange={(e) => {
          handlePasswordChange(e.target.value);
        }}
      />
    </div>
  );
}
