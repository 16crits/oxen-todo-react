import { useState } from "react";

export function ConfirmPassword({ password, handlePasswordChange }) {
  const [showPasswordWarning, setshowPasswordWarning] = useState("hidden");
  const [confirmPassword, setConfirmPassword] = useState("");

  return (
    <div>
      <div className="flex justify-between">
        <label htmlFor="confirmPassword">Confirm Password</label>
        <label
          htmlFor="confirmPassword"
          className="text-red-500"
          style={{ visibility: showPasswordWarning }}
        >
          Passwords doesn't match.
        </label>
      </div>

      <input
        id="confirmPassword"
        type="password"
        onChange={(e) => {
          setConfirmPassword(e.target.value);
          handlePasswordChange(
            e.target.value,
            password !== e.target.value && e.target.value !== ""
              ? "visible"
              : "hidden"
          );
          password !== e.target.value && e.target.value !== ""
            ? setshowPasswordWarning("visible")
            : setshowPasswordWarning("hidden");
        }}
        className={
          password !== confirmPassword && confirmPassword !== ""
            ? "warning"
            : ""
        }
      />
    </div>
  );
}
