import { useState } from "react";

export function Username({ handleNameChange }) {
  const [name, setName] = useState("");
  const [showUserNameWarning, setShowUserNameWarning] = useState("hidden");
  return (
    <div>
      <div className="flex justify-between">
        <span>
          Username <span className="text-red-500">*</span>
        </span>
        <span
          className="text-red-500 font-semibold"
          style={{ visibility: showUserNameWarning }}
        >
          Username is required.
        </span>
      </div>

      <input
        id="name"
        type="text"
        onChange={(e) => {
          setName(e.target.value);
          setShowUserNameWarning("hidden");
          handleNameChange(e.target.value);
        }}
        onBlur={() =>
          !name
            ? setShowUserNameWarning("visible")
            : setShowUserNameWarning("hidden")
        }
        onFocus={() => setShowUserNameWarning("hidden")}
      />
    </div>
  );
}
