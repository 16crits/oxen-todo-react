import { useState } from "react";
import useRegisterRepostiory from "../Composable/useRegisterRepository";

export function Email({ handleEmailChange }) {
  const [email, setEmail] = useState("");
  const [showEmailWarning, setShowEmailWarning] = useState("hidden");
  const [showInvalidEmailWarning, setShowInvalidEmailWarning] =
    useState("hidden");
  const { validateEmail } = useRegisterRepostiory();

  return (
    <div>
      <div className="flex justify-between">
        <span>
          Email <span className="text-red-500">*</span>
        </span>
        <span
          className="text-red-500 font-semibold"
          style={{
            visibility:
              showInvalidEmailWarning === "visible"
                ? showInvalidEmailWarning
                : showEmailWarning,
          }}
        >
          {showEmailWarning === "visible"
            ? " Email is required."
            : showInvalidEmailWarning === "visible"
            ? "Email address is invalid."
            : ""}
        </span>
      </div>
      <input
        id="email"
        type="email"
        onFocus={() => {
          setShowEmailWarning("hidden");
        }}
        onChange={(e) => {
          setEmail(e.target.value);
          handleEmailChange(
            e.target.value,
            !validateEmail(e.target.value) ? "visible" : "hidden"
          );
          !validateEmail(e.target.value)
            ? setShowInvalidEmailWarning("visible")
            : setShowInvalidEmailWarning("hidden");
        }}
        onBlur={() => {
          !email
            ? setShowEmailWarning("visible")
            : setShowEmailWarning("hidden");
        }}
      />
    </div>
  );
}
