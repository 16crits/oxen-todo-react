import { useState } from "react";
import { useSelector } from "react-redux";
import { Link, Navigate } from "react-router-dom";
import useRegisterRepostiory from "./Composable/useRegisterRepository";
import { Username, Email, Password, ConfirmPassword } from "./Components";

export function Register() {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [showPasswordWarning, setshowPasswordWarning] = useState("hidden");
  const [showInvalidEmailWarning, setShowInvalidEmailWarning] =
    useState("hidden");
  const { register, status } = useRegisterRepostiory();
  const { accessToken } = useSelector((state) => state.token);

  return status === 200 ? (
    <Navigate to="/dashboard" />
  ) : (
    <form className="auth-wrapper" onSubmit={(e) => e.preventDefault()}>
      <div>
        <span>Create New User</span>
      </div>
      <Username handleNameChange={(username) => setName(username)} />
      <Email
        handleEmailChange={(email, warningState) => {
          setEmail(email);
          setShowInvalidEmailWarning(warningState);
        }}
      />
      <Password handlePasswordChange={(password) => setPassword(password)} />
      <ConfirmPassword
        password={password}
        handlePasswordChange={(confirmPassword, warningState) => {
          setConfirmPassword(confirmPassword);
          setshowPasswordWarning(warningState);
        }}
      />
      <div>
        <button
          disabled={
            !name ||
            !email ||
            !password ||
            !confirmPassword ||
            showPasswordWarning === "visible" ||
            showInvalidEmailWarning === "visible"
              ? true
              : false
          }
          className="submit"
          onClick={() => register(name, email, password)}
        >
          Register
        </button>
      </div>
      <div>
        <span>
          Already registered? Log in <Link to="/">here</Link>.
        </span>
      </div>
    </form>
  );
}
