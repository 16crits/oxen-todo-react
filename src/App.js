import logo from './logo.svg';
import './App.css';
import { Login, Register } from "./Auth"
import { Index } from "./ToDo"
import { BrowserRouter as Router, Navigate, Route, Routes } from 'react-router-dom';
import { useEffect } from 'react';
import { useSelector } from "react-redux"

function App() {
  const { accessToken } = useSelector((state) => state.token)
  return (
    <div className="app-wrapper">
      <Router>
        <Routes>
          <Route path="register" element={accessToken ? <Navigate to="/dashboard" /> : <Register />} />
          <Route path="/" element={accessToken ? <Navigate to="dashboard" /> : <Login />} />
          <Route path="dashboard" element={!accessToken ? <Navigate to="/" /> : <Index />} />
        </Routes>
      </Router>
    </div>

  );
}

export default App;
